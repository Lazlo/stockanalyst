import java.io.*;
import java.net.CookieManager;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by stefanlatzko on 04.07.17.
 */
public class CSVDownloader {

    private CookieManager cookieManager = new CookieManager();

    public CSVDownloader(String symbol) {
        long timeMillis = System.currentTimeMillis();
        long timeSeconds = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
        URLConnection connection = getConnection("https://finance.yahoo.com/quote/" + symbol + "/history");

        String cookie = getCookie(connection);
        String crumb = extractCrumb(connection);

        URLConnection connectionDownload = getConnection("https://query1.finance.yahoo.com/v7/finance/download/" + symbol + "?period1=728262000&period2=" + timeSeconds + "&interval=1d&events=history&crumb=" + crumb);

        System.out.println(cookie);
        System.out.println("crumb: " + crumb);

        connectionDownload.setRequestProperty("Cookie", cookie);

        try {
            connectionDownload.connect();
            InputStream inputStream = connectionDownload.getInputStream();
            copyInputStreamToFile(inputStream, new File(symbol + ".csv"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copyInputStreamToFile(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            // 2^20 => 128kB => StackOverflow to improve copying performance
            byte[] buf = new byte[1048576];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String extractCrumb(URLConnection connection) {
        InputStream inStream = null;
        try {
            inStream = connection.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        InputStreamReader irdr = new InputStreamReader(inStream);
        BufferedReader rsv = new BufferedReader(irdr);

        Pattern crumbPattern = Pattern.compile(".*\"CrumbStore\":\\{\"crumb\":\"([^\"]+)\"\\}.*");

        String line = null;
        String crumb = null;
        try {
            while (crumb == null && (line = rsv.readLine()) != null) {
                Matcher matcher = crumbPattern.matcher(line);
                if (matcher.matches())
                    crumb = matcher.group(1);
            }
            rsv.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return crumb;
    }

    private String getCookie(URLConnection connection) {
        String cookie = "";

        for (Map.Entry<String, List<String>> entry : connection.getHeaderFields().entrySet()) {
            if (entry.getKey() == null || !entry.getKey().equals("Set-Cookie")) {
                continue;
            }
            cookie = entry.getValue().get(0).split(";")[0];
        }

        return cookie;
    }

    private URLConnection getConnection(String urlString) {
        try {
            URL url = new URL(urlString);
            return url.openConnection();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
