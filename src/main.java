/**
 * Created by stefanlatzko on 04.07.17.
 */
public class main {

    /**
     * @param args <ol>
     *             <li>[0]: stock symbol</li>
     *             </ol>
     */
    public static void main(String[] args) {
        String symbol = "SPY";
        if (args.length > 0 && !args[0].equalsIgnoreCase("")) {
            symbol = args[0];
        }
        new CSVDownloader(symbol);
    }
}
